# Oscar Pérez
# 2022-05-09

(

if [ "$1" == ""  ] ; then
	echo "USO: $0 <dominio> "
	exit
fi


DOMAIN=$1

# Elimina default_server

sed 's/default_server//' -i /etc/nginx/conf.d/*.conf

# reemplaza listen 80
sed 's#listen *80 *;#include /etc/nginx/conf.d/https.inc;#' -i /etc/nginx/conf.d/*.conf

# Elimina ::80

sed 's/listen.*:80 *;//' -i /etc/nginx/conf.d/*.conf


# Crea el archivo 80.conf


cat <<"EOF" >         /etc/nginx/conf.d/80.conf

        server {
          listen 80 default_server ;
	  listen [::]:80 default_server ;

          allow all;

          return 301 http://$host:443$request_uri;

        }
EOF




cat <<"EOF" | sed "s/__DOMAIN__/$DOMAIN/"  >	/etc/nginx/conf.d/https.inc

	listen 443 ssl;
	listen [::]:443 ssl;

	gzip off;

	ssl_certificate /etc/letsencrypt/live/__DOMAIN__/fullchain.pem;
	ssl_certificate_key /etc/letsencrypt/live/__DOMAIN__/privkey.pem;


	location /__my_ip {
		return 200 "$remote_addr"; 
	}

EOF



)  1>&2
