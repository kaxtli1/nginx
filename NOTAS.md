


## Tareas

[ ] Implementar aplicaciones PHP dónde el vlumen solo pertenece a PHP y no Nginx
     
nginx no necesita los archivos .php pues no lo va a procesar, así que no se montan.


https://stackoverflow.com/questions/51900221/is-it-possible-to-pass-request-to-php-fpm-without-nginx-having-volume-mounted


## Diferencia del ofical
https://raw.githubusercontent.com/nginxinc/docker-nginx/594ce7a8bc26c85af88495ac94d5cd0096b306f7/mainline/buster/Dockerfile

El oficial se crea en debian a partir delos paquetes oficiales.

   # arches officialy built by upstream
            echo "deb https://nginx.org/packages/mainline/debian/ buster nginx" >> /etc/apt/sources.list.d/nginx.list \
            && apt-get update \


## Módulos

Hay dos imágenes:

1. nginx. Por lo pronto es gorda, pero la construiré como delgada.
2. nginx-pam. Realiza autenticación con PAM.


No se cual es la mejor estrategia para trabajar con módulos.

1. Imagen gorda. Instalar todos los módulos, la instanciación los activa.

2. Imagen delgada. No instalar ningún módulo. La aplicación instala lo que necesite.

Supongo que debe ser la imagen delgada. Pero

1. La depuración. ¿El comportamiento monolítico disminuye la incertidumbre?

2. Gorda para desarrollo. Delgada para producción. Pero el ambiente de desarrollo debe ser igual al de producción,
asi que llamaré ambiente de laboratorio.




# Diseño

Para usarse en contenedores se tiene el siguiente diseño;

- /etc/nginx/conf.d/ Volumen montado que contiene la configuración de la app


El contenedor nginx require configuración en dos momentos:

- construcción (build ) con la configuración requerida por la app.
- Instanciación ( init ) con el nombre de dominio y certificado SSL

Para el primer caso, la configuración requerida para enginx **se guarda en la imagen de la app** 
y se monta en tiempo de ejecución.

Para el segundo caso, el nombre de dominio y certificado SSL se definen en tiempo de instanciación
**init** 

## Certificado SSL

Las aplicaciones deben configurar el servidor en el **puerto 80** sin SSL. No debe usar la directiva **default_server**.

El instalador agregará una línea con listen 8080:


	/etc/nginx/conf.d/app.conf

	server {
                listen 80;
		listen 8080;
		server_name _ ;
		
		...
	}


El certificado SSL se procesará en otra **capa** con mínima alteración a app.conf.

Al momento de obtener un certificado, se creará un archivo ssh.conf, que redirigirá el tráfico, sin  de la siguiente forma
		
	/etc/nginx/conf.d/ssh.conf


	# Contexto http

	# Por seguridad, se bloque el acceso a las redes externas ( app.conf usa el puerto 80 )
	allow 127.0.0.1;
	allow 192.168.0.0/16;
	allow 172.16.0.0/12;
	allow 10.0.0.0/8;
	deny all;

	# Redirije el puerto 80 a una conexión segura
	server {
	  listen 80 default_server ;
	  allow all;

	  return 301 http://$host:443$request_uri;

	}


	# # El servidor ssl usa un proxy_pass
	server {
	  listen 443 ;
	  allow all;

	#  return 200 "muy bien $host\n";

	location / {
      	  proxy_pass              http://localhost:8080;
          proxy_set_header X-Forwarded-For $remote_addr;
	  proxy_set_header Host       $host;
	}


}

Esta solución:

- Pro: La configuración es realiada con poca intrusión 
- Pro: Conceptualmente se separa la lógica de app.conf con la de ssl.conf
- Con: Hay una carga de redirección conel proxy_pass
- Con: el bloqueo del puerto 80 no es evidente en app.conf

### Version 2


Sustituir en app.conf el listen 80


	sed 's#listen *80 *;#include /etc/nginx/conf.d/https.inc;#' -i /etc/niginx/conf.d/*.conf

	/etc/nginx/conf.d/app.conf

        server {

                include /etc/nginx/conf.d/https.inc ;  # Línea sustituida
                server_name _ ;

                ...
        }


	/etc/nginx/conf.d/80.conf

	
        # Redirije el puerto 80 a una conexión segura
        server {
          listen 80 default_server ;
          allow all;

          return 301 http://$host:443$request_uri;

        }

	/etc/nginx/conf.d/https.inc

	listen 443 ssl;

	ssl_certifcate 
	ssl_key 

	location /__my_ip {
		return 200 "$remote_addr"; 
	}



# keepalive

Para verificar que el servidor está arriba se utiliza la estrategia de android. para http y https.



